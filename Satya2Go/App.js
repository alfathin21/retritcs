import React, {Component} from 'react';
import Router from './src/config/route/index';
console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];
console.disableYellowBox = true;
export default class Home extends Component {
  render() {
    return <Router />;
  }
}

