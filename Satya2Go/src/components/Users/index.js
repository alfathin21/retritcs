import React, {Component} from 'react';
import {
  Container,
  Content,
  Text,
  Thumbnail,
} from 'native-base';
import {View, TouchableOpacity,AsyncStorage} from 'react-native';

export default class User extends Component {

  constructor(props) {
    super(props);
    this.state = {
        username:'',
        nama:'',
        email:'',
        panggilan:''
    };
    this.user();
}
  user = async () => {
    try{
      let user = await AsyncStorage.getItem('Username');
      fetch('http://admin-2go.satyadm.co.id/service/product/profile/'+user)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          temp: responseJson,
          nama:responseJson.nama,
          email:responseJson.email,
          panggilan:responseJson.panggilan,
          username: user
        });
      })
      .catch((error) =>{
        // console.error(error);
      });
    }
    catch(error){
    }
  }
  render() {
    return (
        <Content>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              backgroundColor: '#F0FFFF',
              padding: 12,
            }}>

            <Text style={{fontSize: 13, textAlign:'center', paddingTop:20}}>
             {this.state.username+' / '+this.state.nama}
            </Text>
            
         
          </View>          
        </Content>
    );
  }

  
}