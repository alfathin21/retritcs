import React from 'react';
import {View, Image, Text} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

const Dasboard = props => {
  return (
    
    <View style={{marginRight: 18}}>
      <View style={{width: 290, height: 170}}>
        <Image
          source={props.img}
          style={{
            width: undefined,
            height: undefined,
            resizeMode: 'cover',
            flex: 1,
            borderRadius: 3,
          }}
        />
        <View
          style={{
            position: 'absolute',
            bottom: 0,
            left: 0,
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: 16,
            paddingBottom: 10,
          }}>
          <View>
            <Text
              style={{
                fontSize: 25,
                fontWeight: 'bold',
                color: 'white',
                marginBottom: 5,
              }}>
              {props.title1}
            </Text>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
            {props.title2}
            </Text>
          </View>
          <View style={{flex: 1, paddingLeft: 10}}>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Dasboard;