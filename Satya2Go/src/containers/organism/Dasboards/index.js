import React, {Component} from 'react';
import {View, Image, Text, ScrollView} from 'react-native';
import Dasboard from '../../../components/Dasboard';

class Dasboards extends Component {
  render() {
    return (
      <View>
        <View style={{padding: 12}}>
          <ScrollView horizontal style={{flexDirection: 'row', paddingLeft: 1, showsVerticalScrollIndicator:'false'}}>
            <Dasboard
              img={require('../../../assets/Image/clients.png')}
              title1="24"
              title2="Total Clients"
            />
            <Dasboard
              img={require('../../../assets/Image/products.png')}
              title1="720"
              title2="Total Products"
            />
            <Dasboard
              img={require('../../../assets/Image/scanActivity.png')}
              title1="0"
              title2="Scan Activity"
            />
            <Dasboard
              img={require('../../../assets/Image/readyStock.png')}
              title1="89"
              title2="Ready Stock"
            />
          </ScrollView>
        </View>
      </View>
    );
  }
}

export default Dasboards;