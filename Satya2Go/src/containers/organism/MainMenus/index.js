import React, {Component} from 'react';
import {View,Dimensions} from 'react-native';
import {withNavigation} from 'react-navigation';
import Menus from '../../../components/molucul/Menus/index';


const {width, height} = Dimensions.get('window');

class MainMenus extends Component {
  render() {
    return (
        <View style={{flexGrow:1}}>
          <View style={{flex:1, justifyContent:'center',marginBottom:30, marginTop:70}}>
          <Menus
          onPress={() => this.props.navigation.navigate('Inventory')}
          img={require('../../../assets/Image/StockOpname.png')}
          />
          </View>
          <View style={{flex:1,justifyContent:'center',marginTop:29,marginTop:50}}>
          <Menus onPress={() => this.props.navigation.navigate('Support')}
          img={require('../../../assets/Image/support.png')}/>         
          </View>
        </View>
    );
  }
}

export default withNavigation(MainMenus);
