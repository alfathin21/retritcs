import React, {Component} from 'react';
import {
  AppRegistry,
  Text,
  View,
  StyleSheet,
  StatusBar,
  Image,
  Dimensions,
  AsyncStorage
} from 'react-native';

export default class SplashScreen extends Component {
  constructor() {
    super();
    this._signin();
  }
  render() {
    return (
      <View
      style={{
        flex: 1,
        justifyContent: 'flex-end',
      }}>
      <StatusBar
        barStyle='light-content'
        hidden={false}
        backgroundColor='#000'
      />
      <View style={{...StyleSheet.absoluteFill}}>
        <Image
          source={require('../../../assets/Image/bgSplash.png')}
          style={{flex: 1, height: null, width: null}}
          resizeMode='stretch'
        />
      </View>
      <View style={{flex:1, justifyContent:'center',alignItems:'center'}}>
        <Image style={{width:110,height:110}}
        source={require('../../../assets/Image/Satya2Go.png')} />
      </View>
      <View style={{flex:1, justifyContent:'flex-end',alignItems:'center',marginBottom:7}}>
        <Image 
        source={require('../../../assets/Image/SatyaSystem.png')} />
        <Text style={{fontSize:10, fontWeight:'bold', textAlign:'center', color:'#ffff'}}>Versi 0.1.0</Text>
      </View>
    </View>
  );
}


_signin = async () => {
  try{
    let user = await AsyncStorage.getItem('isLoggedIn');
    if (user !== '1' ) {
      setTimeout(() => {
        this.props.navigation.navigate('Login');
      }, 2000);
    }else {
      this.props.navigation.navigate('Home');
    }
  }
  catch(error){
    // alert(error);
  }
}
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
container: {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
},
button: {
  backgroundColor: 'white',
  height: 70,
  marginHorizontal: 20,
  borderRadius: 35,
  alignItems: 'center',
  justifyContent: 'center',
  marginVertical: 5,
},
});

AppRegistry.registerComponent('SplashScreen', () => SplashScreen);
