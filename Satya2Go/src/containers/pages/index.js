import Home from './Home';
import SplashScreen from './SplashScreen';
import Login from './Login';
import ScanQR from './ScanQR'
import Inventory from './Inventory'
import Items from './Items'
import Listitem from './Listitem'
import SearchItem from './SearchItem'
import Support from './Support/index'




export{
    Home,
    SplashScreen,
    Login,
    Inventory,
    Items,
    ScanQR,
    Listitem,
    SearchItem,
    Support,
    
}