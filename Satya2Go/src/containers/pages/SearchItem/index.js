import React, { Component,Fragment} from 'react';
import { View,StyleSheet,TouchableOpacity,FlatList,Image } from 'react-native';
import { Container, Header, Content, DatePicker,Button, Text,Form,Item,Picker } from 'native-base';
import {Appbar} from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';
export default class DatePickerExample extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      selected2: undefined,   
      PickerValueHolder : '',
      dataSource:[],
      detail:false,
      home:false
    };
    this.setDate = this.setDate.bind(this);
  }
  setDate(newDate) {
    this.setState({ 
      exem:moment(newDate).format('YYYY-MM-DD')
     });
  }
  setTo(newDate) {
    this.setState({ 
      axen:moment(newDate).format('YYYY-MM-DD')
     });
  }
  searchAct() {
    var datefrom = this.state.exem;
    var dateto = this.state.axen;
    var so = this.state.PickerValueHolder;
    this.setState({
      detail:true
    })
    let details = {
      'datefrom': datefrom,
      'dateto': dateto,
      'so': so
};
let formBody = [];
    for (let property in details) {
        let encodedKey = encodeURIComponent(property);
        let encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");
    fetch('http://admin-2go.satyadm.co.id/service/product/search', {
      method: 'POST',
      headers: {
          'Authorization': 'Bearer token',
          'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: formBody
  }).then((response) => response.json())
      .then((responseData) => {
        if (responseData != '') {
          this.setState({
            tempDetail: responseData,
          });
        }
      })
      .done();
}

ListEmpty = () => {
  return (
    //View to show when list is empty
    <View style={styles.MainContainer}>
       <Image source={require('../../../assets/Image/not.png')}  style={{width:300, height:300,borderRadius:30, marginTop:10}} />
      <Text style={{ textAlign: 'center' }}>Maaf, Data Tidak Tersedia</Text>
    </View>
  );
};

back = () => {
  this.setState({
    detail: false
})  
}
componentDidMount(){
    var account_nam=[]
    fetch('http://admin-2go.satyadm.co.id/service/product/so')
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({
        isLoading: false,
        dataSource: responseJson
      });

    })
    .catch((error) =>{
      console.error(error);
    });
}




countryList = () =>{
    return( this.countryData.map( (x,i) => { 
          return( <Picker.Item label={x} key={i} value={x}  />)} ));
}


  render() {
    const {detail,home} = this.state;
    return (
      <Fragment>
         {!detail && !home && 
      <Container>
        <Appbar.Header style={styles.header}>
          <Appbar.BackAction icon="camera"
          onPress={() => this.props.navigation.navigate('Inventory')}
          />
              <Appbar.Content
                title="Search"
                subtitle="Pencarian Data History Scan SO"
              />
              <Appbar.Action onPress={() => this.props.navigation.navigate('Inventory')} AwesomeIcon="settings"  />
            </Appbar.Header>
        <Content>
          <DatePicker
            defaultDate={new Date()}
            locale={"en"}
            formatChosenDate={date => {return moment(date).format('DD MMM YYYY');}}
            timeZoneOffsetInMinutes={undefined}
            modalTransparent={false}
            animationType={"fade"}
            androidMode={"default"}
            placeHolderText="Date From :"
            textStyle={{ color: "black" }}
            placeHolderTextStyle={{ color: "black" }}
            onDateChange={this.setDate.bind(this)}
            disabled={false}
            />
             <DatePicker
             style={styles.mj}
            defaultDate={new Date()}
            formatChosenDate={date => {return moment(date).format('DD MMM YYYY');}}
            locale={"en"}
            timeZoneOffsetInMinutes={undefined}
            modalTransparent={false}
            animationType={"fade"}
            androidMode={"default"}
            placeHolderText="Date To :"
            textStyle={{ color: "black" }}
            placeHolderTextStyle={{ color: "black" }}
            onDateChange={this.setTo.bind(this)}
            disabled={false}
            />
            <Form style={styles.frm}>
            <Item picker>
            <Picker
            selectedValue={this.state.PickerValueHolder}
            onValueChange={(itemValue, itemIndex) => this.setState({PickerValueHolder: itemValue})} >
            { this.state.dataSource.map((item, key)=>(
            <Picker.Item label={item.pelanggan_name_so} value={item.pelanggan_name_so} key={key} />)
            )}
    
          </Picker>
            </Item>
          </Form>
          
          <Button  onPress={this.searchAct.bind(this)} style={styles.tmb} primary>
            <Icon style={styles.icn} name="search" />
            <Text>Search Data</Text>
          </Button>
        </Content>
      </Container>
            }

            {detail &&
            
              <Container>
                 <Appbar.Header style={styles.header}>
          <Appbar.BackAction icon="camera"
          onPress={this.back}
          />
              <Appbar.Content
                title="Search Result"
                
              />
            </Appbar.Header>

            <FlatList
                    style={{flex:1}}
                    data={this.state.tempDetail}
                 
                    renderItem={({ item }) => 
                    <TouchableOpacity >
                      <View style={styles.listItem}>
                      <View style={{flex:1,marginLeft:10}}>
                        <Text style={{fontWeight:"bold",lineHeight:20}}>{'Kode Barang : ' + item.barang_kode}</Text>
                        <Text style={{lineHeight:20}}>{'No.PO : ' + item.po}</Text>
                        <Text style={{lineHeight:20}}>{'Nama Barang : '+ item.nama}</Text>
                        <Text style={{lineHeight:20}}>{'Harga : '+item.harga}</Text>
                        <Text style={{lineHeight:20}}>{'No. Barang : '+item.no_barang}</Text>
                        <Text>{'Lokasi SO : '+item.pelanggan_name_so}</Text>
                      </View>
                    </View>
                </TouchableOpacity>
              }
                keyExtractor={item => item.po}
                ListEmptyComponent={this.ListEmpty}
              />
              </Container>
            }
   </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF'
  },
  
header:{
  backgroundColor: '#1E90FF',
},
  listItem:{
    margin:10,
    padding:10,
    backgroundColor:"#EAEAEA",
    width:"90%",
    flex:1,
    alignSelf:"center",
    flexDirection:"row",
    borderRadius:5
  },
  tmb:{
      margin:25,
      padding:10,
      borderRadius:7,
      alignItems:'center',
      justifyContent:'center'
  },
  icn:{
    color:'white',
    fontSize:15,
    fontWeight:'bold'
  },
  frm:{
    margin:5,
  },
  MainContainer: {
   
    justifyContent: 'center',
    alignContent:'center',
    alignItems:'center',
    flex: 1,
    top:0,
    left:0,
    right:0,
    bottom:0,
    margin: 40,
  }
});