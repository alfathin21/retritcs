import React, {Component,Fragment} from 'react';
import {Text, View,AsyncStorage,TouchableOpacity, Image, Dimensions, ActivityIndicator,Linking,StatusBar} from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import styles from './scanStyle';
import { InputAutoSuggest } from 'react-native-autocomplete-search';
const { width } = Dimensions.get('screen');
class ScanQode extends Component {
  constructor(props) {
      super(props);
      this.state = {
          scan: false,
          ScanResult: false,
          result: null,
          value:'',
          dataSource: [],
          tampung: [],
          kode_barang:'',
          nama_barang:'',
          plu:'',
          kodePelanggan:'',
          namaPelanggan:'',
          price:'',
          jumlah:'',
          no_pd:'',
          username:''
      };
      this.user();
  }
  getRemoteData = async (data) => {
    let details = {
        'id': data,
        'kodePelanggan': this.state.kodePelanggan,
        'namaPelanggan': this.state.namaPelanggan,
        'username': this.state.username
  };
  let formBody = [];
      for (let property in details) {
          let encodedKey = encodeURIComponent(property);
          let encodedValue = encodeURIComponent(details[property]);
          formBody.push(encodedKey + "=" + encodedValue);
      }
      formBody = formBody.join("&");
      fetch('http://admin-2go.satyadm.co.id/service/product/items', {
        method: 'POST',
        headers: {
            'Authorization': 'Bearer token',
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: formBody
    }).then((response) => response.json())
        .then((responseData) => {
        if (responseData != '') {
            this.setState({
              kode_barang:responseData.kode_barang,
              nama_barang:responseData.nama_barang,
              plu:responseData.plu,
              jumlah:responseData.jumlah,
              price:responseData.price
              })
          }
        })
        .done();
  }



 
  tes =  (param) => {
   if (param != null) {
    this.setState({
        kodePelanggan: param.id,
        namaPelanggan: param.name
      });
     
   }

  }  


  
user = async () => {
    try{
      let user = await AsyncStorage.getItem('Username');
      this.setState({
        username: user
      });
    }
    catch(error){
    }
  }
  onSuccess = (e) => {
      const check = e.data.substring(0, 4);
      this.setState({
      scan: false,
      ScanResult: true
      })
      if (check === 'http') {
          Linking
              .openURL(e.data);
      } else {
        this.setState({
          scan: false,
          ScanResult: true
          })
          const data = e.data;
          this.getRemoteData(data);
      }
  }

  componentDidMount(){
    fetch('http://admin-2go.satyadm.co.id/service/product/pelanggan')
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({
        isLoading: true,
        tampung: responseJson,
      });
     
    })
    .catch((error) =>{
     // console.error(error);
    });
}



  activeQR = () => {
      if (this.state.kodePelanggan == '') {
        setTimeout(() => {
            alert('Silahkan Pilih Toko !');
          }, 100); 
      }else {
        this.setState({
            scan: true
        })
      }
     
  }
  scanAgain = () => {
      this.setState({
          scan: true,
          ScanResult: false,
          kode_barang:'',
          nama_barang:'',
          plu:'',
          price:'',
          jumlah:''
      })
  }
  render() {
      const { scan, ScanResult, result } = this.state
      const req = this.state.tampung;
      const desccription = 'Silahkan pilih Toko terlebih dahulu, Sebelum Melakukan SO !'
      return (
          <View style={styles.scrollViewStyle}>
              <Fragment>
                  <StatusBar barStyle="dark-content" />
                  {!scan && !ScanResult &&
                  <View>
                        <View>
                        <Text style={styles.textTitle}>Pilih Toko </Text> 
                        </View>
                      <View style={styles.cardView} >
                          <Text numberOfLines={8} style={styles.descText}>{desccription}</Text>
                          <InputAutoSuggest
                          staticData={req}
                          flatListStyle={{color:'black',marginBottom:10,}}
                          inputStyle={{color:'black', marginBottom:10, width:230, justifyContent: 'center'}}
                          itemTextStyle={{color:'black'}}
                          itemTagStyle={{color:'black'}}
                          onDataSelectedChange={this.tes}
                          itemFormat={{id: 'details.id', name: 'details.nama'}}
                        />
                          <TouchableOpacity onPress={this.activeQR} style={styles.buttonTouchable}>
                              <Text style={styles.buttonTextStyle}>Click to Scan !</Text>
                          </TouchableOpacity>
                      </View>
                    </View>
                  }

                  {ScanResult &&
                      <Fragment>
                        <Text style={styles.textTitle1}>Data Hasil Scan !</Text>
                        <View style={ScanResult ? styles.scanCardView : styles.cardView}>
                        <Text>Kode Toko SO : {` ${this.state.kodePelanggan}`}</Text>
                        <Text>Pelanggan Toko SO : {` ${this.state.namaPelanggan}`}</Text>
                        <Text>Kode Barang : {` ${this.state.kode_barang}`}</Text>
                        <Text>Nama Barang :{` ${this.state.nama_barang}`}</Text>
                        <Text>Kode PLU: {` ${this.state.plu}`}</Text>
                        <Text>Harga (Rp.) : {` ${this.state.price}`}</Text>
                        <Text>Jumlah Barang : {` ${this.state.jumlah}`}</Text>
                        <TouchableOpacity onPress={this.scanAgain} style={styles.buttonTouchable2}>
                        <Text style={styles.buttonTextStylE}>Lakukan Scan Lagi</Text>
                        </TouchableOpacity>
                          </View>
                      </Fragment>
                  }
                  {scan &&
                      <QRCodeScanner
                          reactivate={true}
                          showMarker={true}
                          ref={(node) => { this.scanner = node }}
                          onRead={this.onSuccess}
                          topContent={
                              <Text style={styles.centerText}>
                              <Text style={styles.textBold}>Dekatkan Barcode</Text>
                               </Text>
                          }
                          bottomContent={
                              <View>
                                  <TouchableOpacity style={styles.buttonTouchablE} onPress={() => this.setState({ scan: false })}>
                                      <Text style={styles.buttonTextStylE}>Stop Scan</Text>
                                  </TouchableOpacity>
                              </View>
                          }
                      />
                  }
              </Fragment>
          </View>

      );
  }
}



export default ScanQode;