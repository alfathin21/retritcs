import React, { Component,Fragment } from 'react';
import { StyleSheet,StatusBar, Text, View, FlatList, Image, TouchableOpacity, ActivityIndicator } from 'react-native';
import {Appbar} from 'react-native-paper';

export default class Items extends Component {
  constructor(props){
    super(props);
    this.state = { 
      loading: false,
      error: null,
      detail:false,
      home:false,
      opini:false,
      penjualan_no:'',
      penjualans:''
    }
  }

componentDidMount(){
      fetch('http://admin-2go.satyadm.co.id/service/product/barang')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: true,
          dataSource: responseJson,
        });

      })
      .catch((error) =>{
        // console.error(error);
      });
}
 
activeQR = (penjualan_no) => {
    this.setState({
      detail: true,
      penjualan_no:penjualan_no
  })  
  fetch('http://admin-2go.satyadm.co.id/service/product/detail/'+penjualan_no)
  .then((response) => response.json())
  .then((responseJson) => {
    this.setState({
      tempDetail: responseJson,
    });
  })
  .catch((error) =>{
    // console.error(error);
  });



}
back = () => {
  this.setState({
    detail: false
})  




} 

up = () => {
  this.setState({
    detail: true,
    opini:false
})  
} 

 opi = (no,kode) => {
  this.setState({
    detail: false,
    home:false,
    opini:true,
    penjualans:no
})  
fetch('http://admin-2go.satyadm.co.id/service/product/detailP/'+no+'/'+kode)
  .then((response) => response.json())
  .then((responseJson) => {
    this.setState({
      temp: responseJson,
    });

  })
  .catch((error) =>{
     console.error(error);
  });
} 


render(){
    const {opini,detail,home} = this.state;
    if (this.state.loading) {
      return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
      <View style={styles.container}>
          <Fragment>
              {!detail && !home && !opini &&
               <Fragment>
                <Appbar.Header style={styles.header}>
                <Appbar.BackAction icon="camera"
                onPress={() => this.props.navigation.navigate('Inventory')}
                />
                <Appbar.Content
                  title="LIST"
                  subtitle="Data List Order 30 Data Penjualan"
                />
              </Appbar.Header>
              <FlatList
                style={{flex:1}}  
                data={this.state.dataSource}
                renderItem={({ item }) => 
                <TouchableOpacity 
                onPress={(penjualan_no) => this.activeQR(item.penjualan_no) }>
                <View style={styles.listItem}>
                  <View style={{flex:1}}>
                    <Text style={{lineHeight:20,fontWeight:'bold', fontSize:18}}>{item.no}</Text>
                    <Text style={{lineHeight:20}}>{ item.pelanggan_kode}</Text>
                    <Text style={{lineHeight:20}}>{item.tgl}</Text>
                    <Text style={{lineHeight:20}}>{item.nama}</Text>
                    <Text style={{lineHeight:20}}>{ item.alamat}</Text>
                    <Text style={{lineHeight:20}}>{item.telp}</Text>
                    <Text>{item.email}</Text>
                  </View>
                </View>
                </TouchableOpacity>
              }
                keyExtractor={item => item.no}
              />
               </Fragment>
                }
                {detail &&
                  <Fragment>
                    <Appbar.Header style={styles.header}>
                    <Appbar.BackAction icon="camera"
                      onPress={this.back}
                        />
                    <Appbar.Content
                      title="LIST DETAILS"
                      subtitle={'Penjualan Detail : ' + this.state.penjualan_no}
                    />
                  </Appbar.Header>
                  <FlatList
                    style={{flex:1}}
                    data={this.state.tempDetail}
                    renderItem={({ item }) => 
                    <TouchableOpacity 
                    onPress={(no) => this.opi(this.state.penjualan_no,item.kode_barang) }>
                    <View style={styles.listItem}>
                    <Image source={require('../../../assets/icons/ScanQR1.png')}  style={{width:60, height:60,borderRadius:30, marginTop:10}} />
                      <View style={{flex:1,marginLeft:10}}>
                        <Text style={{lineHeight:20,fontWeight:"bold",fontSize:18}}>{item.kode_barang}</Text>
                        <Text style={{lineHeight:20}}>{item.nama_barang}</Text>
                        <Text style={{lineHeight:20}}>{'Jumlah :' +item.jumlah}</Text>
                      </View>
                    </View>
                 
                </TouchableOpacity>
              }
                keyExtractor={item => item.no}
              />
                </Fragment>
                }




{opini &&
    <Fragment>
        <Appbar.Header style={styles.header}>
                    <Appbar.BackAction icon="camera"
                      onPress={this.up}
                        />
                    <Appbar.Content
                      title="Details  SELECTED"
                      subtitle={'No. Penjualan : ' + this.state.penjualans}
                    />
                  </Appbar.Header>
                  <FlatList
                    style={{flex:1}}
                    data={this.state.temp}
                    renderItem={({ item }) => 
                   
                    <View style={styles.listItem}>
                      <View style={{flex:1}}>
                      <Text style={{lineHeight:20,fontWeight:"bold",fontSize:18}}>{item.no}</Text>
                        <Text style={{lineHeight:20}}>{item.kode_barang}</Text>
                        <Text style={{lineHeight:20}}>{item.nama_barang}</Text>
                        <Text style={{lineHeight:20}}>{item.plu}</Text>
                        <Text style={{lineHeight:20}}>{'Rp.' +item.harga}</Text>
                      </View>
                    </View>
              }
                keyExtractor={item => item.no}
              />
    </Fragment>                 
}
              </Fragment>
      </View>
   
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF'
  },
  header:{
    backgroundColor: '#1E90FF',
  },
  listItem:{
    margin:10,
    padding:10,
    backgroundColor:"#EAEAEA",
    width:"95%",
    flex:1,
    alignSelf:"center",
    flexDirection:"row",
    borderRadius:10
  }
});
