import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  Image,
  Dimensions
} from 'react-native';

export default class Support extends Component {

  render() {
    return (
      <View
      style={{
        flex: 1,
        justifyContent: 'flex-end',
      }}>
      <StatusBar
        barStyle='light-content'
        hidden={false}
        backgroundColor='#000'
      />
      <View style={{...StyleSheet.absoluteFill}}>
        <Image
          source={require('../../../assets/Image/bgSplash.png')}
          style={{flex: 1, height: null, width: null,}}
          resizeMode='stretch'
        />
      </View>
      <View style={{flex:1, justifyContent:'flex-start',alignItems:'center',paddingTop:20}}>
        <Image style={{width:110,height:110}}
        source={require('../../../assets/Image/Satya2Go.png')} />
        <View style={{flex:1, justifyContent:'center', alignItems:'center',paddingHorizontal:4}}>
                <Text style={{fontSize:20,fontWeight:'bold',color:'red', paddingTop:15}}>Address</Text>
                    <Text style={{textAlign:'center', fontSize:15, color:'#FFF'}}>Green Sedayu Bizpark Blok DM1 No.68</Text>
                    <Text style={{textAlign:'center', fontSize:15, color:'#FFF'}}>Jl. Daan Mogot KM 18 Kalideres - Jakarta,Indonesia 11850.</Text>
                <Text style={{fontSize:20,fontWeight:'bold',color:'red', paddingTop:15}}>Contact</Text>
                    <Text style={{textAlign:'center', fontSize:15, color:'#FFF'}}>Phone : +6221-229-52-602</Text>
                    <Text style={{textAlign:'center', fontSize:15, color:'#FFF'}}>Fax : +6221-229-52-611</Text>
                <Text style={{fontSize:20,fontWeight:'bold',color:'red', paddingTop:15}}>E-mail</Text>
                    <Text style={{textAlign:'center', fontSize:15, color:'#FFF'}}>Sales : sales@satyadm.co.id</Text>
                    <Text style={{textAlign:'center', fontSize:15, color:'#FFF'}}>Info : info@satyadm.co.id</Text>
                    <Text style={{textAlign:'center', fontSize:15, color:'#FFF'}}>Custumer Service : cs@satyadm.co.id</Text>    
        </View>
      </View>
    </View>
  );
}
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
container: {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
},
button: {
  backgroundColor: 'white',
  height: 70,
  marginHorizontal: 20,
  borderRadius: 35,
  alignItems: 'center',
  justifyContent: 'center',
  marginVertical: 5,
},
});