import React, {Component} from 'react';
import {ScrollView, Text, View, StatusBar, Dimensions, StyleSheet, Image} from 'react-native';
import MainMenus from '../../organism/MainMenus';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { AsyncStorage } from 'react-native';



export default class Home extends Component {
  render() {
    return (
      <View
      style={{
        flex: 1,
        justifyContent: 'flex-end',
      }}>
      <StatusBar
        barStyle='light-content'
        hidden={false}
        backgroundColor='#000'
      />
      {/* Background*/}
      <View style={{...StyleSheet.absoluteFill}}>
        <Image
          source={require('../../../assets/Image/bgSplash.png')}
          style={{flex: 1, height: null, width: null}}
          resizeMode='stretch'
        />
      </View>
      {/* Logo */}
      <View style={{flex:1, justifyContent:'flex-start',alignItems:'center', marginTop:23}}>
        <Image style={{width:110,height:110}}
        source={require('../../../assets/Image/Satya2Go.png')} />
      </View>
      <MainMenus /> 
      <View style={{justifyContent:'flex-end',alignItems:'center', paddingTop:70}}>
          <TouchableOpacity onPress={this._logout}>
            <Text style={{width:300,
                              height:50,
                              backgroundColor:'rgba(255, 255,255,0.2)',
                              borderRadius: 25,
                              paddingHorizontal:16,
                              fontSize:16,
                              color:'#ffffff',
                              marginVertical: 10,
                              textAlign:'center',paddingTop:13}}>LOGOUT</Text> 
                              </TouchableOpacity>
          </View>        
      {/* Logo&Version info */}
      <View style={{flex:1, justifyContent:'flex-end',alignItems:'center',marginBottom:7}}>
        <Image 
        source={require('../../../assets/Image/SatyaSystem.png')} />
        <Text style={{fontSize:10, fontWeight:'bold', textAlign:'center', color:'#ffff'}}>Versi 0.1.0</Text>
      </View>
    </View>
  );
}

_logout = async () => {
  await AsyncStorage.clear();
  this.props.navigation.navigate('Login');
}

}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
container: {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
},
button: {
  backgroundColor: 'white',
  height: 70,
  marginHorizontal: 20,
  borderRadius: 35,
  alignItems: 'center',
  justifyContent: 'center',
  marginVertical: 5,
},
});

