import React, { Component } from 'react';
import {View,StyleSheet, StatusBar,Text, Dimensions, TouchableOpacity, ScrollView} from 'react-native'
import NavbarIcon from '../../organism/NavbarIcon/index'
import Dasboards from '../../organism/Dasboards/index';
import Users from '../../../components/Users/index'
import {Appbar} from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';
const {width, height} = Dimensions.get('window');
class Inventory extends Component {
  _handleSearch = () => {
    this.props.navigation.navigate('SearchItem');
  };
    render() {
        return(
    <View style={{flex:1, backgroundColor:'#FFF'}}>
        <StatusBar
        barStyle='dark-content'
        hidden={false}
        backgroundColor='#1E90FF'
      />
       <Appbar.Header style={styles.header}>
        <Appbar.Content
          title="Satya2Go Apps"
          titleStyle={{fontSize:22}}
        />
    <Appbar.Action icon="magnify" onPress={this._handleSearch} />
        </Appbar.Header>        
              <ScrollView style={{flex: 1,marginTop:1}}>
                  <View style={{flex: 1,marginTop:1}}>
                  <Users />
                  </View>
                  <View style={{flex: 1,marginTop:1}}>
                  <Dasboards />
                  </View>
              </ScrollView>
        <NavbarIcon />
    </View>
        )
    }

}
const styles = StyleSheet.create({
 
  header:{
    backgroundColor: '#1E90FF',
  }
});
export default Inventory;