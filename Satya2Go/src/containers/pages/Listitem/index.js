import React, {Fragment} from 'react';
import { StyleSheet, Text, View, FlatList,StatusBar, Image,ActivityIndicator, TouchableOpacity } from 'react-native';
import {Appbar} from 'react-native-paper';


export default class ListItem extends React.Component {
  constructor(props){
    super(props);
    this.state = { 
      loading: false,
      error: null,
      detail:false,
      home:false,
      opini:false
    }
  }
  componentDidMount(){
    fetch('http://admin-2go.satyadm.co.id/service/product/list')
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({
        isLoading: true,
        dataSource: responseJson,
      }, function(){

      });

    })
    .catch((error) =>{
      console.error(error);
    });
}

up = () => {
  this.setState({
    detail: true,
    opini:false
})  
} 


back = () => {
  this.setState({
    detail: false
})  
}


opi = (kode,no,noBarang) => {
  this.setState({
    detail: false,
    home:false,
    opini:true,
    penjualans:no
})  

fetch('http://admin-2go.satyadm.co.id/service/product/lid/'+no+'/'+kode+'/'+noBarang)
.then((response) => response.json())
.then((responseJson) => {
  this.setState({
    temp: responseJson,
  });
})
.catch((error) =>{
  // console.error(error);
});
} 


activeQR = (penjualan_no) => {
  this.setState({
    detail: true
})  


fetch('http://admin-2go.satyadm.co.id/service/product/listd/'+penjualan_no)
.then((response) => response.json())
.then((responseJson) => {
  this.setState({
    tempDetail: responseJson,
  });
})
.catch((error) =>{
  // console.error(error);
});


}

  render(){
    const {opini,detail,home} = this.state;
    if (this.state.loading) {
      return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Fragment>
        {!detail && !home && !opini &&
                  <Fragment>
                  <Appbar.Header style={styles.header}>
                    <Appbar.BackAction icon="camera"
                    onPress={() => this.props.navigation.navigate('Inventory')}
                    />
                    <Appbar.Content
                      title="ITEMS"
                      subtitle="List Items Scan History"
                    />
                    <Appbar.Action onPress={() => this.props.navigation.navigate('Auth')} AwesomeIcon="settings"  />
                  </Appbar.Header>
                  <FlatList
                    style={{flex:1}}
                    data={this.state.dataSource}
                    renderItem={({ item }) =>
                    <TouchableOpacity
                    onPress={(penjualan_no) => this.activeQR(item.po) }
                 >
                  <View style={styles.listItem}>
                  <Image source={require('../../../assets/icons/ScanQR1.png')}  style={{width:60, height:60,borderRadius:30, marginTop:10}} />
                  <View style={{marginLeft:10,flex:1}}>
                    <Text style={{fontWeight:"bold"}}>{item.po}</Text>
                    <Text >{item.barang_kode}</Text>
                    <Text>{'PLU : ' +item.plu}</Text>
                    <Text>{item.nama_barang}</Text>
                    <Text>{'harga : ' + item.harga}</Text>
                    <Text>{'Lokasi SO : ' + item.pelanggan_kode_name_so}</Text>
                  </View>
                    <View style={{backgroundColor:'#FFF',width: 45, height: 45, borderRadius: 40,textAlign:'center',alignContent:'center',marginTop:20}}>
                    <Text style={{textAlign:'center',color:'#1E90FF',marginTop:12}}>{item.jumlah}</Text>
                    </View>
                </View>
                </TouchableOpacity>
                  }
                    keyExtractor={item => item.id}
                  />
                  </Fragment>
  }




{detail &&
<Fragment>
            <Appbar.Header style={styles.header}>  
                <Appbar.BackAction icon="camera"
               onPress={this.back}
                />
                <Appbar.Content
                  title="LIST ITEMS"
                  subtitle="List Items Scan History"
                />
              </Appbar.Header>
              <FlatList
                    style={{flex:1}}
                    data={this.state.tempDetail}
                    renderItem={({ item }) => 
                    <TouchableOpacity 
                    onPress={(no) => this.opi(item.barang_kode,item.po,item.no_barang) }>

                  <View style={styles.listItem}>
                    <Image source={require('../../../assets/icons/ScanQR1.png')}  style={{width:60, height:60,borderRadius:30, marginTop:10}} />
                      <View style={{alignSelf:"center",flex:1,marginLeft:10}}>
                        <Text style={{fontWeight:"bold"}}>{item.barang_kode}</Text>
                      </View>
                      <View style={{backgroundColor:'#FFF',width: 45, height: 45, borderRadius: 40,textAlign:'center',alignContent:'center',marginTop:20}}>
                    <Text style={{textAlign:'center',color:'#1E90FF',marginTop:12}}>{item.no_barang}</Text>
                    </View>
                    </View>
                </TouchableOpacity>
              }
                keyExtractor={item => item.po}
              />
</Fragment>
  }


{opini &&
<Fragment>
<Appbar.Header style={styles.header}>  
                <Appbar.BackAction icon="camera"
                onPress={this.up}
                />
                <Appbar.Content
                  title="ITEMS DETAILS"
                  subtitle="Items Detail Scan History"
                />
              </Appbar.Header>

              <FlatList
                    style={{flex:1}}
                    data={this.state.temp}
                    renderItem={({ item }) => 
                    <TouchableOpacity >
                  <View style={styles.listItem}>
                      <View style={{flex:1,marginLeft:10}}>
                        <Text style={{fontWeight:"bold",lineHeight:20}}>{'Kode Barang : ' + item.barang_kode}</Text>
                        <Text style={{lineHeight:20}}>{'No.PO : ' + item.po}</Text>
                        <Text style={{lineHeight:20}}>{'Nama Barang : '+ item.nama}</Text>
                        <Text style={{lineHeight:20}}>{'Harga : '+item.harga}</Text>
                        <Text style={{lineHeight:20}}>{'No. Barang : '+item.no_barang}</Text>
                        <Text>{'Lokasi SO : '+item.pelanggan_name_so}</Text>
                      </View>
                    </View>
                </TouchableOpacity>
              }
                keyExtractor={item => item.po}
              />
</Fragment>
  }
      </Fragment>
      </View>
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF'
  },
  
header:{
  backgroundColor: '#1E90FF',
},
  listItem:{
    margin:10,
    padding:10,
    backgroundColor:"#EAEAEA",
    width:"90%",
    flex:1,
    alignSelf:"center",
    flexDirection:"row",
    borderRadius:5
  }
});
